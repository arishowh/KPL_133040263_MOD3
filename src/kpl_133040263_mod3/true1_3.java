/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpl_133040263_mod3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author SB604
 */
public class true1_3 {
    public static synchronized void undeclaredThrow(Throwable throwable) throws InstantiationException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
// These exceptions should not be passed
if (throwable instanceof IllegalAccessException ||
throwable instanceof InstantiationException) {
// Unchecked, no declaration required
throw new IllegalArgumentException();
}
NewInstance.throwable = throwable;
try {
Constructor constructor =
NewInstance.class.getConstructor(new Class<?>[0]);
constructor.newInstance();
} catch (InstantiationException e) { /* Unreachable */
} catch (IllegalAccessException e) { /* Unreachable */
} catch (InvocationTargetException e) {
System.out.println("Exception thrown: "
+ e.getCause().toString());
} finally { // Avoid memory leak
NewInstance.throwable = null;
}
}

    private static class NewInstance {

        private static Throwable throwable;

        public NewInstance() {
        }
    }
}
