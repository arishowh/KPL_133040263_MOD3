/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpl_133040263_mod3;

/**
 *
 * @author SB604
 */
public class false1_6 {
    class SuperClass {
public SuperClass () {
doLogic();
}
public void doLogic() {
System.out.println("This is superclass!");
}
}
class SubClass extends SuperClass {
private String color = null;
public SubClass() {
super();
color = "Red";
}
public void doLogic() {
System.out.println("This is subclass! The color is :" + color);
// ...
}
}
public class Overridable {
public  void main(String[] args) {
SuperClass bc = new SuperClass();
// Prints "This is superclass!"
SuperClass sc = new SubClass();
// Prints "This is subclass! The color is :null"
}
}
}
